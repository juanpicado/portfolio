(function ( document, window, C ) {

    'use strict';

	if (Modernizr.touch && !window.location.hash) {
      $(window).load(function () {
		setTimeout(function () {
            window.scrollTo(0, 1);
			}, 0);
		});
    }

	$(document).ready(function() {


    var router = new J.Router();
     var Modernizr = window.Modernizr;

	var bash = {
		command_list :[],
		message_list : []
	};
	$.each(C, function(i, e){
		bash.command_list.push(i);
		bash.message_list.push(e);
	});

    $('.terminal').terminal(function(command, term) {
        if (command !== '') {
            try {
                  term.echo(J.Bash(command, term, bash));
            } catch(e) {
				var r = "" + e;
                term.error(r);
            }
        } else {
           term.echo('');
        }
    }, {
        greetings: '@jotadeveloper &#8226; Front End Engineer Portfolio',
        name: 'js_demo',
        height: 250,
        history : true,
        command_list : bash.command_list,
        tabcompletion : true,
		login: false,
        onInit : function (term) {
			term.echo("_____________________________________________");
			term.echo(" Type 'help' to display the instructions or hire me if you need more help ;) ");
			term.echo("											");
        },
        prompt: 'jota-portfolio$ > '});

	});

	// load index
	var main  = $("#main"),
	home = new J.Views.Home();
    main.empty().append(home.$el);

})(document, window, C);
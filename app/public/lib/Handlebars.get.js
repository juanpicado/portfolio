/*
 * Extends Handlebars with a basic get method for loading external
 * Handlebars templates. Simply pass an options object which contains
 * the following properties:
 * - path    (required) : Path to the external template file to be loaded
 * - success (required) : Callback invoked with the compiled loaded template.
 * - cache   (optional) : true if the template is to be cached, otherwise false.
 *
 * In addition to the above arguments, any jQuery/Zepto.ajax options argument
 * can be specified as well.
 */
Handlebars.get = function( options )
{
    var args = $.extend( {}, options, {
        success : function( data ) {
        options.success( Handlebars.compile( data ) );
    }
   });
   $.ajax( args );
}
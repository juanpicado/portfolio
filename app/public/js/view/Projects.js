(function ( document, window, Backbone, J) {

'use strict';

J.Views = J.Views || {};

J.Views.Projects = Backbone.View.extend({

      initialize: function() {
          this.render();
      },


      clean_video : function () {
         $('#cnVideo').empty();
         $('#daiVideo').empty();
      },

      render: function() {
        var p = this;
        J.util.template('/partials/projects.html', function(source) {
            var template = source({});
            p.$el.append(template);

            $('.close-video').click(p.clean_video);

            // dai
            var $myModal = $('#daiModal').modal({show: false});
            var $myCarousel = $('#myCarousel').carousel({'interval': false});
            $('.dai').click(function(e) {
                  var $this = $(this);
                  $myModal.modal('show');
                  $myCarousel.carousel();
                  p.clean_video();
            });

            //sms banex
            var $smsModal = $('#smsModal').modal({show: false});
            $('.sms-bnx').click(function(e) {
                var $this = $(this);
                $smsModal.modal('show');
            });


            //enicaragua
            var $eModal = $('#eModal').modal({show: false});
            $('.enicaragua-video').click(function(e) {
              var $this = $(this);
              $eModal.modal('show');
              p.clean_video();
              var pop = Popcorn.youtube( "#daiVideo", "http://www.youtube.com/watch?v=p9gvCs2brjM" );
              pop.footnote({
                 start: 0
              });
              pop.play();
            });

            // countrynet
            var $cnModal = $('#cnModal').modal({show: false});
            var $cnCarousel = $('#myCNCarousel').carousel({'interval': false});
            $('.countrynet').click(function(e) {
                  $cnModal.modal('show');
                  $cnCarousel.carousel();
                  p.clean_video();
                  var pop = Popcorn.youtube( "#cnVideo", "http://www.youtube.com/embed/sem81o23FMI" );
                  pop.footnote({
                     start: 0
                  });
            });


          });
      }
  });

})(document, window, Backbone, J);
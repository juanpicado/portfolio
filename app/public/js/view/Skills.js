(function ( document, window, Backbone, J) {

'use strict';

J.Views = J.Views || {};

J.Views.Skills = Backbone.View.extend({

      initialize: function() {
          this.render();
      },

      render: function() {
        var p = this;
        J.util.template('/partials/skill.html', function(source) {
            var template = source({});
            p.$el.append(template);
          });
       }
  });

})(document, window, Backbone, J);
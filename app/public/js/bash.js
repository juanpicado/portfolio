(function ( document, window, J) {

'use strict';

J.Bash = function(action, term, commands) {
var loaded = function (name) {
  term.echo('Loading App : ' + name + '.app');
  term.echo('................................');
  term.echo('Loaded OK!');
};
var result = "";
var i = commands.command_list.indexOf(action);
if ( action === 'help') {
  term.clear();
  term.echo('Portfolio bash, version 0.1.0-release (the cloud)');
  term.echo('The most commonly commands are :');
  term.echo('[[;#FFF;#000]help]         display instructions');
  term.echo('[[;#FFF;#000]home]         back to home');
  term.echo('[[;#FFF;#000]about]        more about me');
  term.echo('[[;#FFF;#000]resume]       display my resume');
  term.echo('[[;#FFF;#000]contact]      contat me');
  term.echo('[[;#FFF;#000]encuestame]   all about encuestame');
  term.echo('[[;#FFF;#000]projects]     all my projects ');
  term.echo('[[;#FFF;#000]twitter]      my twitter');
  term.echo('[[;#FFF;#000]linkedin]     open my linkedin');
  term.echo('[[;#FFF;#000]hired]        Try it on ;)');
} else {
if (i !== -1) {
		var message = commands.message_list[i];
		if (message.bash) {
			var command = message.message;
			term.echo(command);
		} else {
			loaded(action);
			window.location = "/#" + action;
		}
} else {
	term.error('-bash: ' + action + ': command not found, please type help or tweet me @jotadeveloper');
}
}
return result;
};

})(document, window, J);
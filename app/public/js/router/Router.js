(function ( document, window, Backbone, J) {

'use strict';

var main  = $("#main");

J.Router = Backbone.Router.extend({

        initialize: function() {

            // Tells Backbone to start watching for hashchange events
            Backbone.history.start();
        },

        routes: {
            "": "home",
            "home": "home",
            "encuestame" : "encuestame",
            "projects" : "projects",
            "chronology" : "chronology",
            "about" : "about",
            "contact" : "contact",
            "resume" : "resume",
            "skills" : 'skills'
        },

        home : function() {
            var home = new J.Views.Home();
            main.empty().append(home.$el);
        },

        // skills : function () {
        //     var skill = new J.Views.Skills();
        //     main.empty().append(skill.$el);
        // },

        encuestame : function () {
            var home = new J.Views.EnMe();
            main.empty().append(home.$el);
        },

        projects : function () {
            var home = new J.Views.Projects();
            main.empty().append(home.$el);
        },

        chronology : function () {
            var home = new J.Views.Chronology();
            main.empty().append(home.$el);
        },

        contact : function () {
            var home = new J.Views.Contact();
            main.empty().append(home.$el);
        },


        resume : function () {
            var home = new J.Views.Resume();
            main.empty().append(home.$el);
        },

        about : function () {
            var home = new J.Views.About();
            main.empty().append(home.$el);
        }
});

})(document, window, Backbone, J);
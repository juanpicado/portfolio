(function (Handlebars ) {
   'use strict';
	J.util =  J.util || {};
	J.util.template = function (path, callback) {
		Handlebars.get({
			cache : true,
			url : path,
			success : callback
		});
	};
})(Handlebars);
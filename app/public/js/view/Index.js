(function ( document, window, Backbone, J) {

'use strict';

J.Views = J.Views || {};

J.Views.Home = Backbone.View.extend({

      initialize: function() {
          this.render();
      },

      render: function() {
          var p = this;
          J.util.template('/partials/index.html', function(source) {
            var template = source({});
            p.$el.append(template);
          });
          // this.template = _.template(template, this.model.toJSON());
          // this.$el = $(this.template);
          // return this;
        }
  });

})(document, window, Backbone, J);
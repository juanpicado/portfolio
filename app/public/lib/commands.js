(function ( window) {

'use strict';

window.J = window.J || {};
window.C = {
	"home" : {
		"bash" : false,
	},
	"about" : {
		"bash" : false,
	},
	"resume" : {
		"bash" : false,
	},
	// "skills" : {
	// 	"bash" : false,
	// },
	"encuestame" : {
		"bash" : false,
	},

	"contact" : {
		"bash" : false,
	},

	"projects" : {
		"bash" : false,
	},

	"dojo" : {
		"bash" : true,
		"message" : "My favourite framework of course, the current version is 1.9"
	},

	"twitter" : {
		"bash" : true,
		"message" : "http://www.twitter.com/jotadeveloper"
	},

	"email" : {
		"bash" : true,
		"message" : "jotadeveloperDOTgmail.com"
	},

	"jotadeveloper" : {
		"bash" : true,
		"message" : "my nick name on internet"
	},

	"github" : {
		"bash" : true,
		"message" : "My github profile :: https://github.com/juanpicado"
	},

	"linkedin" : {
		"bash" : true,
		"message" : "My linkedin profile :: http://at.linkedin.com/in/jotadeveloper"
	},


	"hired" : {
		"bash" : true,
		"message" : "Oh really ?? I hope you have Macbook Pro in your office."
	},


	"ebook" : {
		"bash" : true,
		"message" : "My favourite book, High Performance JavaScript by Nicholas C. Zakas, 2010"
	},

	"bash" : {
		"bash" : true,
		"message" : "Is my bash, follow my rules"
	},


	"version" : {
		"bash" : true,
		"message" : "portfolio 0.1.0"
	},

	"jsf" : {
		"bash" : true,
		"message" : "No way, I hate JSF http://ihatejsf.com/"
	},


	"backbone" : {
		"bash" : true,
		"message" : "This site is made with Backbone ;) 0.9.10"
	},

	"git" : {
		"bash" : true,
		"message" : "Of course, I use Git always, if you are using svn, please ignore my profile"
	},


	"spring" : {
		"bash" : true,
		"message" : "My best Java Framework, I'm spring fan boy"
	},


	"macbook" : {
		"bash" : true,
		"message" : "If you try to hire me, please be sure you have one in your office for me."
	},


	"html5" : {
		"bash" : true,
		"message" : "I always thought that html5 is a html4 add-on."
	},


	"svg" : {
		"bash" : true,
		"message" : "RaphaelJS is the best and my favourite"
	},

	"design" : {
		"bash" : true,
		"message" : "Im not a web designer, I'm a Javascript Engineer"
	},


	"certifications" : {
		"bash" : true,
		"message" : "I hate it, please give me a book, I'm self taught"
	},


	"php" : {
		"bash" : true,
		"message" : "Long time ago since my last php code, but I still like it"
	},


	"java" : {
		"bash" : true,
		"message" : "A long experience with Java, but I prefer to be a Front End Engineer"
	},


	"node" : {
		"bash" : true,
		"message" : "Learning all about Node.js, right now a Node.js rookie"
	},


	"require" : {
		"bash" : true,
		"message" : "Are you refered to RequireJS, very good library to manage modules, my prefered"
	},


	"responsive" : {
		"bash" : true,
		"message" : "Responsive Design, of course, please visite http://www.encuestame.org from your mobile device"
	},


	"leader" : {
		"bash" : true,
		"message" : "I like to think I am"
	},

	"mobile" : {
		"bash" : true,
		"message" : "My next challenge of course"
	},

	"hibernate" : {
		"bash" : true,
		"message" : "Hibernate, the best ORM"
	},


	"compass" : {
		"bash" : true,
		"message" : "Sure, this site is made with compass"
	},

	"grunt" : {
		"bash" : true,
		"message" : "Sure, this site is made with grunt 0.4"
	},


	"javascript" : {
		"bash" : true,
		"message" : "Trust me, I'm Front-End Engineer"
	}

};
})( window );
# My portfolio

Build with **Grunt.js**

**Libraries used**

* Twitter Bootstrap 2.3.x
* jQuery 1.9 + Plugins
* Modernizr 2.6.x
* Handlebars
* Popcorn 1.3
* Underscore 1.4.x
* Backbone 0.9.2
* grunt.js 0.4.1
* grunt-usermin 0.1.11


### How to build

    npm install
    grunt 
    grunt dist
    
### Dev mode

    npm install
    grunt dev
    
     
**Live demo**
  
[http://www.jotadeveloper.es](http://www.jotadeveloper.es)